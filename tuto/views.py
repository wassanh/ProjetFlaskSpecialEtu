from .app import app
from flask import render_template

@app.route("/")
def home():
    return render_template(
        "home.html",
        title="Hello world avec template",
        names=["Pierre","Paul","Jacques"]
    ) 

#Exercice 7
import yaml
data = yaml.safe_load(open("data.yml"))

@app.route("/books")
def books():
    return render_template(
        "books.html",
        title = 'Liste de livres',
        books = data
    )